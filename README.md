# Virtual Machine Halt 

Virtual Machine Halt (VMHalt) is a Linux kernel module that detects if is running in a virtual or emulated environment and halt the machine boot.

VMHalt was conceived to use over an ecosystem where students would perform a practical exam in their personal computer, and it was unintended that they may cheat by running the exam over a virtual machine/emulator and use the installed distribution.

VMHalt uses multiple types of signature, cache and timing attacks over a special instruction in kernel space to detect if it is running over a virtual environment.

To compile, use the make file and install the module.

Details of the methods used to detect virtualization or emulation are described in more detail in our paper. It can be found in the following link:
    
https://dl.acm.org/citation.cfm?id=2851948

Virtualization and Emulation are a fast evolving field, so the techniques may not work over future upgrades of current virtualizers/emulators nor in future virtualization methodologies.

If you use this module or improve it in your research, please give cite us as indicated down below:

    @inproceedings{Reis:2016:VMH:2851613.2851948,
     author = {Reis, Sim\~{a}o and Z\'{u}quete, Andr{\'e} and Vieira, Jos{\'e}},
     title = {Virtual Machine Halt},
     booktitle = {Proceedings of the 31st Annual ACM Symposium on Applied Computing},
     series = {SAC '16},
     year = {2016},
     isbn = {978-1-4503-3739-7},
     location = {Pisa, Italy},
     pages = {1903--1905},
     numpages = {3},
     url = {http://doi.acm.org/10.1145/2851613.2851948},
     doi = {10.1145/2851613.2851948},
     acmid = {2851948},
     publisher = {ACM},
     address = {New York, NY, USA},
     keywords = {caches, privileged instructions, sensitive instructions, system emulators, virtual machines},
    } 
