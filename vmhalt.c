/**
 * "Virtal Machine Halt" kernel module
 *
 * Simão Reis <dracostriker@hotmail.com>
 *
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/reboot.h>
#include <linux/sched.h>
#include <asm/cacheflush.h>
#include <asm/cpufeature.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/msr.h>
#include <asm/page.h>
#include <asm/processor.h>
#include <asm/tlbflush.h>

/** Auxiliar macro functions */

#define smp_another_processor_id() !smp_processor_id()
#define enable_cache()                       \
 	__asm__("push   %rax\n\t"                \
            "mov    %cr0,%rax;\n\t"          \
            "and     $~(1 << 30),%rax;\n\t"  \
            "mov    %rax,%cr0;\n\t"          \
            "wbinvd\n\t"                     \
            "pop    %rax"                    \
            )
#define disable_cache()                    \
 	__asm__("push   %rax\n\t"              \
            "mov    %cr0,%rax;\n\t"        \
            "or     $(1 << 30),%rax;\n\t"  \
            "mov    %rax,%cr0;\n\t"        \
            "wbinvd\n\t"                   \
            "pop    %rax"                  \
            )

/** Known virtual CPU signatures */

#define KVM_SIGNATURE    "KVMKVMKVM"
#define VMWARE_SIGNATURE "VMwareVMware"
#define HYPERV_SIGNATURE "Microsoft Hv"
#define XEN_SIGNATURE    "XenVMMXenVMM"
#define GET_CPU_SIGNATURE         0x40000000 /* CPU signature */
#define GET_EFER_REGISTER         0xC0000080 /* Extended Feature Enable Register */
#define GET_PERFORMANCE_COUNTER_0 0x0        /* Read Performance Monitor Counter 0 */

/** Parallel counter test threshouls */

#define CPUID_COUNTER_THRESHOLD       235
#define RDMSR_EFER_COUNTER_THRESHOLD  199
#define INB_COUNTER_THRESHOLD        1235

/** Relative performance test threshouls */

#define CPUID_TIME_THRESHOLD       51
#define RDMSR_EFER_TIME_THRESHOLD  45
#define INB_TIME_THRESHOLD        750

/* Cache effects */

#define RUN_SAMPLES 6
#define CACHE_TIME_THRESHOLD 20

/* I/O ports */

#define BASEPORT 0x378 /* lp1 */

/** Test 1 - Detect hypervisor. */

static int
hypervisor_test(void)
{
    unsigned int eax, ebx, ecx, edx;
    static char signature[13] = {'\0'};
    if (cpu_has_hypervisor)
    {
        cpuid(GET_CPU_SIGNATURE, &eax, &ebx, &ecx, &edx);
        memcpy(signature + 0, &ebx, 4);
        memcpy(signature + 4, &ecx, 4);
        memcpy(signature + 8, &edx, 4);
        signature[12] = '\0';
        printk("Hypervisor detected, ");
        if (strcmp(signature, KVM_SIGNATURE) == 0)
            printk("running KVM platform\n");
        else if (strcmp(signature, VMWARE_SIGNATURE) == 0)
            printk("running VMware platform\n");
        else if (strcmp(signature, HYPERV_SIGNATURE) == 0)
            printk("running Microsoft's HyperV platform\n");
        else if (strcmp(signature, XEN_SIGNATURE) == 0)
            printk("running Xen HVM platform\n");
        else
            printk("running an unknown platform\n");
    	printk("CPU Signature: %s\n", signature);
    }
    else
        printk("Hypervisor not detected\n");
    return cpu_has_hypervisor;
}

/** Test 2 - Parallel Counter. */

static volatile int stopCounter, startCounter;
static volatile uint64_t startValue, endValue, counter;
static uint64_t cpuid_Count, rdmsr_efer_Count, inb_Count;

static int
cpuid_thread(void *data)
{
    unsigned int eax, ebx, ecx, edx;
    local_irq_disable();
    while (!startCounter);
    startValue = counter;
    cpuid(GET_CPU_SIGNATURE, &eax, &ebx, &ecx, &edx); // #VMEXIT
    endValue = counter;
    stopCounter = 1;
    local_irq_enable();
    return 0;
}

static int
rdmsr_efer_thread(void *data)
{
    uint32_t low, high;
    local_irq_disable();
    while (!startCounter);
    startValue = counter;
    rdmsr(GET_EFER_REGISTER, low, high); // #VMEXIT
    endValue = counter;
    stopCounter = 1;
    local_irq_enable();
    return 0;
}

static int
inb_thread(void *data)
{
    local_irq_disable();
    while (!startCounter);
    startValue = counter;
    inb(BASEPORT + 1); // #VMEXIT
    endValue = counter;
    stopCounter = 1;
    local_irq_enable();
    return 0;
}

static uint64_t
parallel_counter_thread(int (*task)(void *))
{
    struct task_struct *task_counter;
    local_irq_disable();
    stopCounter = 0;
    startCounter = 0;
    counter = 0;
    task_counter = kthread_run(task, NULL, "paralel_thread");
    kthread_bind(task_counter, smp_another_processor_id());
    startCounter = 1;
    while (!stopCounter)
        counter++;
    kthread_stop(task_counter);
    local_irq_enable();
    return endValue - startValue;
}

static int
parallel_counter_detect(int (*task)(void *), uint64_t threshold, char *instruction, uint64_t *count)
{
    *count = parallel_counter_thread(task);
    printk("%s counter: %llu\n", instruction, *count);
    return *count > threshold ? 1 : -1;
}

static int
single_cpu_emulator_test(void)
{
    int emu = 0;
    emu = cpuid_Count == 0 && rdmsr_efer_Count == 0 && inb_Count == 0;
    if (emu)
        printk("All counters at 0: emulator running over a single CPU detected!\n");
    return emu;
}

static int
parallel_counter_test(void)
{
    int vm = 0;
    vm += parallel_counter_detect(&cpuid_thread,      CPUID_COUNTER_THRESHOLD,      "CPUID",      &cpuid_Count);
    vm += parallel_counter_detect(&rdmsr_efer_thread, RDMSR_EFER_COUNTER_THRESHOLD, "RDMSR EFER", &rdmsr_efer_Count);
    vm += parallel_counter_detect(&inb_thread,        INB_COUNTER_THRESHOLD,        "INB",        &inb_Count);
    printk("Parallel counter voting: %d\n", vm);
    return single_cpu_emulator_test() || vm > 0;
}

/** Test 3 - Reserved or invalid MSR. */

static int
invalid_instruction_test(void)
{
    int emu = 0;
    uint64_t p;
    emu |= !rdmsrl_safe(0x18B, &p); // read invalid MSR
    if (emu)
        printk("Valid msr: emulator detected\n");
    else 
        printk("Invalid msr: hardware detected\n");
    return emu;
}

/** Test 4 - Relative performance. */

static unsigned int rel_cpuid, rel_rdmsr, rel_inb;

static int
virtual_box_signature_test(void)
{
    int emu = 0;
    emu = rel_cpuid < 4 && rel_rdmsr < 4 && rel_inb < 4;
    if (emu)
        printk("All rel lesser than 4: Virtual Box detected!\n");
    return emu;
}

static int
relative_perfomance_test(void)
{
    int emu = 0;
    uint64_t time, time_nop, time_cpuid, time_rdmsr, time_inb;
    unsigned int eax, ebx, ecx, edx;
    uint32_t low, high;
    local_irq_disable();
    time = __native_read_tsc();
    __asm__("nop");	
    time_nop = __native_read_tsc() - time;
    time = __native_read_tsc();
    cpuid(GET_CPU_SIGNATURE, &eax, &ebx, &ecx, &edx);
    time_cpuid = __native_read_tsc() - time;
    time = __native_read_tsc();
    rdmsr(GET_EFER_REGISTER, low, high);
    time_rdmsr = __native_read_tsc() - time;
    time = __native_read_tsc();
    inb(BASEPORT + 1);
    time_inb = __native_read_tsc() - time;
    local_irq_enable();
    emu += (rel_cpuid = time_cpuid / time_nop) > CPUID_TIME_THRESHOLD ? 1 : -1;
    emu += (rel_rdmsr = time_rdmsr / time_nop) > RDMSR_EFER_TIME_THRESHOLD ? 1 : -1;
    emu += (rel_inb   = time_inb   / time_nop) > INB_TIME_THRESHOLD ? 1 : -1;
    printk("NOP time = %llu\n", time_nop);
    printk("CPUID time = %llu\n", time_cpuid);
    printk("RDMSR time = %llu\n", time_rdmsr);
    printk("INB time = %llu\n", time_inb);
    printk("CPUID rel = %u\n", rel_cpuid);
    printk("RDMSR rel = %u\n", rel_rdmsr);
    printk("INB rel = %u\n", rel_inb);
    printk("Relative performance voting: %d\n", emu);
    return virtual_box_signature_test() || emu > 0;
}

/** Test 5 - Relative cache performance. */

static int
relative_cache_performance_test(void)
{
	int vm;
	uint64_t run_on_samples[RUN_SAMPLES], run_off_samples[RUN_SAMPLES], startValue, endValue, on_average = 0, off_average = 0, rel;
	int i, a, b;
	enable_cache();
	local_irq_disable();
	for (i = 0; i < RUN_SAMPLES; i++)
	{
		startValue = __native_read_tsc();
		a = b + i; // arbitrary arithmetic operation
		endValue = __native_read_tsc();
		run_on_samples[i] = endValue - startValue;
	}
	local_irq_enable();
	__flush_tlb();
	flush_cache_all();
	disable_cache();
	local_irq_disable();
	for (i = 0; i < RUN_SAMPLES; i++)
	{
		startValue = __native_read_tsc();
		a = b + i; // arbitrary arithmetic operation
		endValue = __native_read_tsc();
		run_off_samples[i] = endValue - startValue;
	}
	local_irq_enable();
	enable_cache();
	for (i = 0; i < RUN_SAMPLES; i++)
		printk("Run %d time: %llu\n", i, run_on_samples[i]);
	for (i = 0; i < RUN_SAMPLES; i++)
		printk("Run %d time: %llu\n", i, run_off_samples[i]);
	for (i = 0; i < RUN_SAMPLES; i++)
		on_average += run_on_samples[i];
	on_average /= RUN_SAMPLES;
	for (i = 0; i < RUN_SAMPLES; i++)
		off_average += run_off_samples[i];
	off_average /= RUN_SAMPLES;
	vm = (rel = off_average / on_average) < CACHE_TIME_THRESHOLD;
	printk("Cache rel = %llu\n", rel);
	if (vm)
		printk("Cache enabled: VM detected\n");
	else
		printk("Cache disabled: hardware detected\n");
	return vm;
}

/**
 * Main function.
 */
static int __init
vm_halt_init(void)
{
    int vm = 0;
    printk("Init Virtual Machine Halt\n");
    vm |= hypervisor_test();
    vm |= parallel_counter_test();
    vm |= invalid_instruction_test();
    vm |= relative_perfomance_test();
    vm |= relative_cache_performance_test();
    if (vm)
    {
        printk("VM found, halt\n");
        kernel_halt();
    }
    else
        printk("VM not found, continue\n");
    return 0;
}

/**
 * This function has no purpose.
 */
static void __exit
vm_halt_exit(void)
{
    printk("Exit Virtual Machine Halt\n");
}

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Simão Reis");
MODULE_DESCRIPTION("Halt kernel at virtual machine or emulator detection.");
module_init(vm_halt_init);
module_exit(vm_halt_exit);
